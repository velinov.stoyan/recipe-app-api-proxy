# Recipe App API Proxy

## Usage

### Environment Variables
 * `LISTEN_PORT` - Port to listen on (default:`8000`)
 * `APP_HOST`  - Host name of the app to forward requests (default:`app`)
 * `APP_PORT` - Port of the app to forward requests ( default:`9000`)

